context('API Tests', () => {
    beforeEach(() => {
        cy.startGame('Berlin');
    });

    let resStone = 'Icon of Stone'
    let resOil = 'Icon of Oil'
    let resOre = 'Icon of IronOre'
    let resCopperOre = 'Icon of CopperOre'

    let resStoneCount = 0
    let resOreCount = 0
    let resCopperOreCount = 0
    let resOilCount = 0

    let resStoneAmount = 0
    let resOreAmount = 0
    let resCopperOreAmount = 0
    let resOilAmount = 0


    describe('API Turns', () => {

        var result
        let url = 'http://localhost:8080/turns'

        it('Validate the header', () => {
            result = cy.request({
                method: 'POST',
                url: url,
                body: {
                    "PlayerName": "Berlin",
                    "action": "BuildIronPlateFactory",
                    "coordinates": {
                        "X": 0,
                        "Y": 0
                    }
                }

            })

            result.its('status')
                .should('equal', 200);
        })      
    })

    describe('API Bank', () => {

        var result
        let url = 'http://localhost:8080/bank'

        it('Validate the header', () => {
            result = cy.request(url)

            result.its('headers')
                .its('content-type')
                .should('include', 'text/plain')
        })

        it('Validate the status', () => {
            result = cy.request(url)

            result.its('status')
                .should('equal', 200);
        })

        it('Validate the body ', () => {
            result = cy.request(url)

            result.its('body')
                .should('include', "Resource")
        })
    })

    describe('API World', () => {

        var result
        let url = 'http://localhost:8080/world'

        it('Validate the header', () => {
            result = cy.request(url)

            result.its('headers')
                .its('content-type')
                .should('include', 'text/plain')
        })

        it('Validate the status', () => {
            result = cy.request(url)

            result.its('status')
                .should('equal', 200);
        })

        it('Validate the body ', () => {
            result = cy.request(url)

            result.its('body')
                .should('include', "tiles")
        })

        it('Validate resources in Json ', () => {
            result = cy.request(url)
                .then((response) => {
                    var json = JSON.parse(response.body)

                    json.tiles.forEach(function (element) {
                        element.forEach(function (element2) {

                            let amount = element2.resourceSite.Amount

                            switch (element2.resourceSite.Resource) {
                                case resOil:
                                    resOilCount++
                                    resOilAmount += amount
                                    break;
                                case resOre:
                                    resOreCount++
                                    resOreAmount += amount
                                    break;
                                case resCopperOre:
                                    resCopperOreCount++
                                    resCopperOreAmount += amount
                                    break;
                                case resStone:
                                    resStoneCount++
                                    resStoneAmount += amount
                                    break;
                                default:
                            }
                        });
                    });

                    expect(resStoneCount).to.be.greaterThan(0)
                    expect(resOilCount).to.be.greaterThan(0)
                    expect(resOreCount).to.be.greaterThan(0)
                    expect(resCopperOreCount).to.be.greaterThan(0)

                    expect(resStoneAmount).to.be.greaterThan(0)
                    expect(resCopperOreAmount).to.be.greaterThan(0)
                    expect(resOreAmount).to.be.greaterThan(0)
                    expect(resOilAmount).to.be.greaterThan(0)
                })
        })
    })
});