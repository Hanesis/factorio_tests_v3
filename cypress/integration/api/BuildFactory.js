context('Factory API Tests', () => {
    beforeEach(() => {
        cy.startGame('Berlin');
    });

    let resStone = 'Icon of Stone';

    describe('Steel factory', () => {

        var result
        let url = 'http://localhost:8080/turns'

        it('Build Facotry', () => {
            cy.mineFirstResourcesXtimes(5, resStone)

            cy.tickGame()

            cy.tickGame()
            
            result = cy.request({
                method: 'POST',
                url: url,
                body: {
                    "PlayerName": "Berlin",
                    "action": "BuildIronPlateFactory",
                    "coordinates": {
                        "X": 0,
                        "Y": 0
                    }
                }

            })
            
            result.its('status')
                .should('equal', 200);
        })

        it('Build Facotry without resources', () => {                   
            result = cy.request({
                method: 'POST',
                url: url,
                body: {
                    "PlayerName": "Berlin",
                    "action": "BuildIronPlateFactory",
                    "coordinates": {
                        "X": 0,
                        "Y": 0
                    }
                }

            })

            cy.on('window:alert', (str) => {
                expect(str).to.equal(alertMsg)
              })

        })
    })    
});