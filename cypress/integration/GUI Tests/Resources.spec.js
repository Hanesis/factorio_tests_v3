context('Resources Tests', () => {
    beforeEach(() => {
        cy.startGame('Berlin');
    });

    let resStone = 'Icon of Stone';
    let resOil = 'Icon of Oil';
    let resOre = 'Icon of IronOre';
    let resCopperOre = 'Icon of CopperOre';
    let IPFactory = "BuildIronPlateFactory";
    let alertMsg = "You need 5 stone to build something!"

    describe('Build Mine with 5 stones is possible', () => {
        it('Stone Mine', function () {
            cy.mineFirstResourcesXtimes(5,resStone)
            
            cy.findFirstResource(resStone).click();
            
            cy.buildMine()        
                       
            cy.findFirstObject("StoneMine")
        })

        it('Iron Ore Mine', function () {
            cy.mineFirstResourcesXtimes(5,resStone)
           
            cy.findFirstResource(resOre).click();

            cy.tickGame();

            cy.buildMine()
            
            cy.findFirstObject("IronOreMine")
        })

        it('Copper Ore Mine', function () {
            cy.mineFirstResourcesXtimes(5,resStone)

            cy.findFirstResource(resCopperOre).click();

            cy.tickGame();

            cy.buildMine()
           
            cy.findFirstObject("CopperOreMine")
        })

        it('Oil Mine', function () {
            cy.mineFirstResourcesXtimes(5,resStone)
           
            cy.findFirstResource(resOil).click();
           
            cy.tickGame();

            cy.buildMine()
            
            cy.findFirstObject("OilMine")

        })
    })

    describe('Imposible to build mine with less than 5 stones', () => {
        it('Stone Mine', function () {
            cy.mineFirstResourcesXtimes(4,resStone)
            cy.tickGame();

            cy.findFirstResource(resStone).click();
            cy.buildMine()  
            
            cy.on('window:alert', (str) => {
                expect(str).to.equal(alertMsg)
            })
        })

        it('Iron Ore Mine', function () {
            cy.mineFirstResourcesXtimes(3,resOre)
            cy.tickGame()

            cy.findFirstResource(resOre).click();
            cy.buildMine()

            cy.on('window:alert', (str) => {
                expect(str).to.equal(alertMsg)
              })
        })

        it('Copper Ore Mine', function () {
            cy.mineFirstResourcesXtimes(2,resCopperOre)

            cy.tickGame()

            cy.findFirstResource(resCopperOre).click();
            cy.buildMine()

            cy.on('window:alert', (str) => {
                expect(str).to.equal(alertMsg)
              })
        })

        it('Oil Mine', function () {
            cy.mineFirstResourcesXtimes(1,resOil)
            cy.tickGame()

            cy.findFirstResource(resOil).click();
            cy.buildMine()

            cy.on('window:alert', (str) => {
                expect(str).to.equal(alertMsg)
              })
        })
    })

    describe('Correct Resources in Deposit', () => {

        let resAmount = 3

        it('Stone Production', function () {
            cy.mineFirstResourcesXtimes(resAmount,resStone)

            cy.tickGame();

            cy.resourceDeposit(resStone).should('be.equals', resAmount.toString());
        })

        it('Iron Ore Production', function () {
            cy.mineFirstResourcesXtimes(resAmount,resOre)

            cy.tickGame();

            cy.resourceDeposit(resOre).should('be.equals', resAmount.toString());
        })

        it('Copper Ore Production', function () {
            cy.mineFirstResourcesXtimes(resAmount,resCopperOre)

            cy.tickGame();

            cy.resourceDeposit(resCopperOre).should('be.equals', resAmount.toString());
        })

        it('Oil Production', function () {
            cy.mineFirstResourcesXtimes(resAmount,resOil)

            cy.tickGame();

            cy.resourceDeposit(resOil).should('be.equals', resAmount.toString());
        })
    })        
});