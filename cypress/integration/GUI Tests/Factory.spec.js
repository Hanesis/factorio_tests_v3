context('Factory Tests', () => {
    beforeEach(() => {
        cy.startGame('Berlin');
    });

    let resStone = 'Icon of Stone';
    let resOil = 'Icon of Oil';
    let resOre = 'Icon of IronOre';
    let resCopperOre = 'Icon of CopperOre';
    let IPFactory = "BuildIronPlateFactory";
    let alertMsg = "You need 5 stone to build something!"
    let IronPlate = "IronPlate"

    describe('Build factory with less than 5 stones is impossible', () => {
        it('Init Build SteelPlates factory', function () {
            cy.mineFirstResourcesXtimes(2, resStone)
           
            cy.findFirstEmptySpot().click();            

            cy.buildIronPlateFactory();

            cy.on('window:alert', (str) => {
                expect(str).to.equal(alertMsg)
              })
        })
    })

    describe('Build factory', () => {
        it('Build SteelPlates factory', function () {
            cy.mineFirstResourcesXtimes(5, resStone)
           
            cy.findFirstEmptySpot().click();

            cy.tickGame();

            cy.tickGame();

            cy.buildIronPlateFactory();
        })
    })

    describe('Factory Consuption', () => {
        it('IronPlate factory consupt 3 Iron Ores to make one steel', function () {
            cy.mineFirstResourcesXtimes(5, resStone)    
           
            cy.tickGame();

            cy.tickGame();
            
            cy.findFirstEmptySpot().click(); 

            cy.buildIronPlateFactory();

            cy.mineFirstResourcesXtimes(3, resOre)

            cy.tickGame();

            cy.tickGame()            

            cy.resourceDeposit(IronPlate).should('be.equals', '1');
        })
    })

});