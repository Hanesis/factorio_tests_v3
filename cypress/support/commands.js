// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This is will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })



Cypress.Commands.add('startGame', (playerName, options = {}) => {

    cy.request({
        method: 'POST',
        url: 'http://localhost:8080/restart'
    });
    cy.visit('/');
    cy.get('select').select(playerName);
    cy.get('button').click(); // play as default player
});

Cypress.Commands.add('getWorld', (options = {}) => {

    cy.request({
        method: 'GET',
        url: 'http://localhost:8080/world'
    });
});

Cypress.Commands.add('tickGame', (playerName, options = {}) => {
    cy.wait(1000); // game ticks like that
});

Cypress.Commands.add('mineResource', (playerName, options = {}) => {
    cy.get('#action-MineResource').click();
});

Cypress.Commands.add('buildFactory', (factory, options = {}) => {

    cy.get('#action-BuildIronPlateFactory').click();
    //cy.get('#action-' + factory).click();
});

Cypress.Commands.add('findFirstEmptySpot', (options = {}) => {

    return cy.get('td > div > img[alt="Icon of None"]:first');
});

Cypress.Commands.add('findFirstResource', (resource, options = {}) => {

    return cy.get('td > div > img[alt="' + resource + '"]:first');
});

Cypress.Commands.add('findFirstObject', (object, options = {}) => {

    return cy.get('td > div > img[alt="' + object + ' Icon"]:first');
});

Cypress.Commands.add('mineFirstResourcesXtimes', (amount, resource, options = {}) => {

    for (let i = 0; i < amount; i++) {
        cy.findFirstResource(resource).click();

        cy.mineResource();

        cy.tickGame()
    }
});
/*
Cypress.Commands.add('findFirstResourceByApi', (resource, options = {}) => {

    let json
    let result = cy.request('http://localhost:8080/world')
        .then((response) => {
            json = JSON.parse(response.body)
                       
            for (let i = 0; i < 29; i++) {
                for (let j = 0; j < 49; j++) {

                    var res = json.tiles[i][j]

                    if (res.resourceSite.Resource == resource) {
                        console.log(i)
                        console.log(j)
                        console.log(res)
                        return j;
                    }
                }
            }
        }).then((some)=>{
            console.log(some)
        })
        
});*/

Cypress.Commands.add('resourceDeposit', (resource, options = {}) => {
    return cy.get('#bank-' + resource + '-amount').invoke('text');
});

Cypress.Commands.add('buildMine', (playerName, options = {}) => {
    cy.tickGame()

    cy.get('#action-BuildMine').click();

    cy.tickGame();
});

Cypress.Commands.add('findFirstEmptySpot', (playerName, options = {}) => {
    return cy.get('td > div > img[alt="None Icon"]:first');
});

Cypress.Commands.add('buildIronPlateFactory', (playerName, options = {}) => {
    cy.get('#action-BuildIronPlateFactory').click();
});

Cypress.Commands.add('buildCopperPlateFactory', (playerName, options = {}) => {
    cy.get('#action-BuildCopperPlateFactory').click();
});

Cypress.Commands.add('WaitForDeposit', (amount, resource, options = {}) => {

    let deposit = 0;

    do {
        deposit = cy.get('#bank-' + resource + '-amount').invoke('text');

        cy.tickGame()

    } while (deposit != amount)

});


